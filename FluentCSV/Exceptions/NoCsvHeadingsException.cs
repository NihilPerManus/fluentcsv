﻿using System;
using System.Runtime.Serialization;

namespace FluentCSV
{
    [Serializable]
    internal class NoFieldsFoundException : Exception
    {
        public NoFieldsFoundException()
        {
        }

        public NoFieldsFoundException(string message) : base(message)
        {
        }

        public NoFieldsFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoFieldsFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}