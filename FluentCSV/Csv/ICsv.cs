﻿using System;
using System.Collections.Generic;

namespace FluentCSV
{
    public interface ICsv<T> where T : class
    {
        string Delimiter { get; set; }
        string NewLine { get; set; }

        void AddCsvField(string heading, Func<T, string> formula);
        void AddItem(T entry);
        void AddRange(IEnumerable<T> items);
    }
}