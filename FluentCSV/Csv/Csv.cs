﻿using FluentCSV.Csv;
using FluentCSV.Csv.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentCSV
{
    public class Csv<T> : ICsv<T> where T : class
    {
        #region Private Fields

        Dictionary<string, Func<T, string>> fields = new  Dictionary<string, Func<T, string>>();
        List<T> records = new List<T>();

        #endregion


        #region Public Properties

        public string Delimiter { get; set; } = ",";
        public string NewLine { get; set; } = Environment.NewLine;

        #endregion


        #region Public Methods

        /// <summary>
        /// Adds a field to the CSV
        /// </summary>
        /// <param name="heading">The name of the field which appears at the top of the CSV</param>
        /// <param name="formula">The function used to extract the value from each item in the record</param>
        /// <returns>The new state of the CSV</returns>
        public void AddCsvField(string heading, Func<T, string> formula)
        {
            fields.Add(heading, formula);

        }

        /// <summary>
        /// Adds a colection of items to the list of records
        /// </summary>
        /// <param name="items"></param>
        /// <returns>The new state of the CSV</returns>
        public void AddRange(IEnumerable<T> items)
        {
            records.AddRange(items);
        }

        /// <summary>
        /// Adds an item to the list of records
        /// </summary>
        /// <param name="entry"></param>
        /// <returns>The new state of the CSV</returns>
        public void AddItem(T entry)
        {
            records.Add(entry);
        }

        /// <summary>
        /// Generates the final output of all objects
        /// </summary>
        /// <returns>CSV-formatted text of all entries</returns>
        public override string ToString()
        {
            //scrapped the idea of returning an empty string, and instead will throw an error if there are no headings 

            if (!fields.Keys.Any())
                throw new NoFieldsFoundException("You must add fields to this object");

            string heading = GetHeading();
            string body = GetRecords();

            return $"{heading}{NewLine}{body}";
        }

        #endregion


        #region Private Methods

        /// <summary>
        /// Returns the Heading part of the CSV in CSV Format.
        /// </summary>
        /// <returns></returns>
        private string  GetHeading()
        {           
            return string.Join(Delimiter, fields.Keys.Select(k => k.ToCsvString()));
        }
        /// <summary>
        /// Returns the records portion of the CSV in CSV format
        /// </summary>
        /// <param name="entries"></param>
        /// <returns></returns>
        private string GetRecords()
        {
            return string.Join(
                NewLine,
                 records.Select(i => string.Join(Delimiter,fields.Keys.Select(k => $"{fields[k].Invoke(i).ToCsvString()}"))                         
                ));    
        }

        #endregion
    }
}
