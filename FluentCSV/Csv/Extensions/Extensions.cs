﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FluentCSV.Csv.Extensions
{

    public static class CsvExtension
    {
        /// <summary>
        /// An extension for creating a Csv object with the records populated. Not sure if this really assists. I just really wanted to make a static class and extension.
        /// </summary>
        public static Csv<T> ToCsv<T>(this IEnumerable<T> me)
        where T : class
        {
            //Create the Csv Item
            Csv<T> csv = new Csv<T>();

            //Add all the items from the IEnumerable
            csv.AddRange(me);

            return csv;
        }

        /// <summary>
        /// Converts a string into a CSV-safe string.
        /// </summary>
        /// <param name="me"></param>
        /// <returns></returns>
        public static string ToCsvString(this string me)
        {
            return $"\"{me.Replace("\"", "\"\"")}\"";
        }


        public static Csv<T> AddField<T>(this Csv<T> me, string heading, Func<T, string> formula)
            where T : class
        {
            me.AddCsvField(heading, formula);
            return me;
        }

        public static Csv<T> AddEntry<T>(this Csv<T> me, T entry)
            where T : class
        {
            me.AddItem(entry);

            return me;
        }

        public static Csv<T> AddEntryRange<T>(this Csv<T> me, IEnumerable<T> entry)
        where T : class
        {
            me.AddRange(entry);

            return me;
        }




    }
    
}
